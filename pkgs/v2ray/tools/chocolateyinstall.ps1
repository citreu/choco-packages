﻿$ErrorActionPreference = 'Stop';

$version     = $env:ChocolateyPackageVersion
$packageName = $env:ChocolateyPackageName
$toolsDir    = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$installDir  = $toolsDir + $packageName
$url         = ("https://github.com/v2ray/v2ray-core/releases/download/v{0}/v2ray-windows-32.zip" -f $version)
$url64       = ("https://github.com/v2ray/v2ray-core/releases/download/v{0}/v2ray-windows-64.zip" -f $version)

$packageArgs = @{
    packageName   = $packageName
    unzipLocation = $installDir
    fileType      = 'exe'
    url           = $url
    url64bit      = $url64
    checksum      = '7af7ad535404c7fb82a7d9aa312ba3e8de71cde9e11eed62a767ef311118a8ee'
    checksumType  = 'sha256'
    checksum64    = '3f87b2ce1772334d0539ec0cd1ca1d50c0e33c3eef6c71f09a277195ac044425'
    checksumType64= 'sha256'
}

Install-ChocolateyZipPackage @packageArgs
